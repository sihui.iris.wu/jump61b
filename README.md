# Jump61B



## Project Description

A board game that mimics the feature of KJumpingCube, along with an opponent AI that has its moves determined using Game Trees and priority Queues. This project is done during the course of CS61B - Data Structures in Fall 2021, taught by Professor Paul Hilfinger in U.C. Berkeley. The full project description can be found here: https://inst.eecs.berkeley.edu/~cs61b/fa21/materials/proj/proj2/index.html
