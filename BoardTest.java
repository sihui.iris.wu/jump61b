package jump61;

import static jump61.Side.*;

import org.junit.Test;
import static org.junit.Assert.*;

/** Unit tests of Boards.
 *  @author Iris Wu
 */

public class BoardTest {

    private static final String NL = System.getProperty("line.separator");

    @Test
    public void testSize() {
        Board B = new Board(5);
        assertEquals("bad length", 5, B.size());
        ConstantBoard C = new ConstantBoard(B);
        assertEquals("bad length", 5, C.size());
        assertEquals("bad1", 1, C.get(0).getSpots());
        assertEquals("bad2", WHITE, C.get(0).getSide());
        Board D = new Board(C);
        assertEquals("bad length", 5, D.size());


    }

    @Test
    public void nullPointerTest() {
        Board E = new Board(5);
        Board F = new ConstantBoard(E);
        F.toString();
    }

    @Test
    public void testSet() {
        Board B = new Board(5);
        B.set(2, 2, 1, RED);
        assertEquals("wrong number of spots", 1, B.get(2, 2).getSpots());
        assertEquals("wrong color", RED, B.get(2, 2).getSide());
        assertEquals("bad", WHITE, B.get(1, 1).getSide());
        assertEquals("wrong count", 1, B.numOfSide(RED));
        assertEquals("wrong count", 0, B.numOfSide(BLUE));
        assertEquals("wrong count", 24, B.numOfSide(WHITE));
    }

    @Test
    public void testMove() {
        Board B = new Board(6);
        checkBoard("#0", B);
        B.addSpot(RED, 1, 1);
        checkBoard("#1", B, 1, 1, 2, RED);
        B.addSpot(BLUE, 2, 1);
        checkBoard("#2", B, 1, 1, 2, RED, 2, 1, 2, BLUE);
        B.addSpot(RED, 1, 1);
        checkBoard("#3", B, 1, 1, 1, RED, 2, 1, 3, RED, 1, 2, 2, RED);
        B.undo();
        checkBoard("#2U", B, 1, 1, 2, RED, 2, 1, 2, BLUE);
        B.undo();
        checkBoard("#1U", B, 1, 1, 2, RED);
        B.undo();
        checkBoard("#0U", B);
    }

    @Test
    public void neighborTest() {
        Board B = new Board(5);
        assertEquals("bad1", 2, B.neighbors(1, 1));
        assertEquals("bad2", 2, B.neighbors(0));
        assertEquals("bad3", 3, B.neighbors(1, 2));
        assertEquals("bad4", 3, B.neighbors(1));
        assertEquals("bad5", 4, B.neighbors(6));
        assertEquals("bad6", 4, B.neighbors(2, 2));
    }

    @Test
    public void addSpotsTest() {
        Board B = new Board(5);
        B.set(2, 2, 3, RED);
        B.set(1, 1, 1, RED);
        B.set(1, 2, 2, RED);
        B.addSpot(RED, 0);
        B.addSpot(RED, 1);
        B.addSpot(RED, 6);
        assertEquals("bad1", 2, B.get(0).getSpots());
        assertEquals("bad2", 3, B.get(1).getSpots());
        assertEquals("bad3", 4, B.get(6).getSpots());

        Board C = new Board(4);
        C.set(1, 1, 2, RED);
        C.set(1, 2, 1, RED);
        C.set(2, 1, 1, BLUE);
        C.addSpot(RED, 0);
        assertEquals("bad4", 1, C.get(0).getSpots());
        assertEquals("bad5", 2, C.get(4).getSpots());
        assertEquals("bad6", 2, C.get(1).getSpots());
        assertEquals("bad7", RED, C.get(0).getSide());
        assertEquals("bad8", RED, C.get(4).getSide());

        Board D = new Board(4);
        D.set(1, 1, 2, RED);
        D.set(1, 2, 3, RED);
        D.set(2, 1, 1, BLUE);
        D.addSpot(RED, 0);
        assertEquals("bad9", 2, D.get(0).getSpots());
        assertEquals("bad10", 2, D.get(4).getSpots());
        assertEquals("bad11", 1, D.get(1).getSpots());
        assertEquals("bad12", 2, D.get(2).getSpots());
        assertEquals("bad13", 2, D.get(5).getSpots());
        assertEquals("bad7", RED, D.get(2).getSide());
        assertEquals("bad8", RED, D.get(5).getSide());
    }

    @Test
    public void initTest() {
        Board B = new Board(2);
        B.set(1, 1, 1, RED);
        B.set(1, 2, 1, RED);
        B.set(2, 1, 1, RED);
        B.set(2, 2, 1, RED);

        Board C = new Board(B);
        checkBoard("bad1", B, 1, 1, 1, RED, 1, 2, 1, RED,
                2, 1, 1, RED, 2, 2, 1, RED);
        checkBoard("bad2", C, 1, 1, 1, RED, 1, 2, 1, RED,
                2, 1, 1, RED, 2, 2, 1, RED);
    }

    @Test
    public void getWinnerTest() {
        Board B = new Board(2);
        B.set(1, 1, 1, RED);
        B.set(1, 2, 1, RED);
        B.set(2, 1, 1, RED);
        B.set(2, 2, 1, RED);
        assertEquals(RED, B.getWinner());

        Board A = new Board(2);
        B.set(1, 1, 1, RED);
        B.set(1, 2, 1, RED);
        B.set(2, 1, 1, RED);
        B.set(2, 2, 1, BLUE);
        assertEquals(null, A.getWinner());

        Board C = new Board(2);
        B.set(1, 1, 1, RED);
        B.set(1, 2, 1, WHITE);
        B.set(2, 1, 1, RED);
        B.set(2, 2, 1, BLUE);
        assertEquals(null, A.getWinner());

        Board D = new Board((2));
        B.set(1, 1, 1, WHITE);
        B.set(1, 2, 1, BLUE);
        B.set(2, 1, 1, RED);
        B.set(2, 2, 1, BLUE);
        assertEquals(null, A.getWinner());
    }

    @Test
    public void copyTest() {
        Board B = new Board(2);
        B.set(1, 1, 1, BLUE);
        B.set(1, 2, 1, BLUE);
        B.set(2, 1, 1, RED);
        B.set(2, 2, 1, BLUE);

        Board C = new Board(2);
        C.set(1, 1, 1, BLUE);
        C.set(1, 2, 1, BLUE);
        C.set(2, 1, 1, RED);
        C.set(2, 2, 1, RED);

        B.copy(C);
        checkBoard("bad1", B, 1, 1, 1, BLUE, 1, 2, 1,
                BLUE, 2, 1, 1, RED, 2, 2, 1, RED);
        checkBoard("bad2", C, 1, 1, 1, BLUE, 1, 2, 1,
                BLUE, 2, 1, 1, RED, 2, 2, 1, RED);

        C.set(1, 1, 1, RED);
        checkBoard("bad3", B, 1, 1, 1, BLUE, 1, 2, 1, BLUE,
                2, 1, 1, RED, 2, 2, 1, RED);
        checkBoard("bad4", C, 1, 1, 1, RED, 1, 2, 1, BLUE,
                2, 1, 1, RED, 2, 2, 1, RED);

        B.set(1, 1, 1, WHITE);
        checkBoard("bad5", B, 1, 2, 1, BLUE,
                2, 1, 1, RED, 2, 2, 1, RED);
        checkBoard("bad6", C, 1, 1, 1, RED,
                1, 2, 1, BLUE, 2, 1, 1, RED, 2, 2, 1, RED);
    }

    @Test
    public void neighborOfTest() {
        Board B = new Board(5);
        int[] result1 = B.neighborOf(0);
        int[] result2 = B.neighborOf(4);
        int[] result3 = B.neighborOf(20);
        int[] result4 = B.neighborOf(24);
        int[] expect1 = new int[]{5, 1};
        int[] expect2 = new int[]{9, 3};
        int[] expect3 = new int[]{15, 21};
        int[] expect4 = new int[]{19, 23};
        assertArrayEquals("bad1", result1, expect1);
        assertArrayEquals("bad2", result2, expect2);
        assertArrayEquals("bad3", result3, expect3);
        assertArrayEquals("bad4", result4, expect4);

        int[] result5 = B.neighborOf(1);
        int[] result6 = B.neighborOf(5);
        int[] result7 = B.neighborOf(21);
        int[] result8 = B.neighborOf(9);
        int[] expect5 = new int[]{0, 2, 6};
        int[] expect6 = new int[]{0, 10, 6};
        int[] expect7 = new int[]{20, 22, 16};
        int[] expect8 = new int[]{14, 4, 8};
        assertArrayEquals("bad5", result5, expect5);
        assertArrayEquals("bad6", result6, expect6);
        assertArrayEquals("bad7", result7, expect7);
        assertArrayEquals("bad8", result8, expect8);
    }

    @Test
    public void currentBoardTest() {
        Board B = new Board(5);
        checkBoard("bad7", B);
        assertEquals(0, B.current());
        B.set(2, 2, 3, RED);
        B.set(1, 1, 1, RED);
        B.set(1, 2, 2, RED);
        B.set(1, 3, 2, RED);

        B.addSpot(RED, 0);
        checkBoard("bad1", B, 1, 1, 2, RED, 2, 2, 3, RED,
                1, 2, 2, RED, 1, 3, 2, RED);

        B.addSpot(RED, 1);
        checkBoard("bad2", B, 1, 1, 2, RED, 2, 2, 3, RED,
                1, 2, 3, RED, 1, 3, 2, RED);

        B.addSpot(RED, 2);
        checkBoard("bad3", B, 1, 1, 2, RED, 2, 2, 3, RED,
                1, 2, 3, RED, 1, 3, 3, RED);

        B.undo();
        checkBoard("bad4", B, 1, 1, 2, RED, 2, 2, 3, RED,
                1, 2, 3, RED, 1, 3, 2, RED);

        B.undo();
        checkBoard("bad5", B, 1, 1, 2, RED, 2, 2, 3, RED,
                1, 2, 2, RED, 1, 3, 2, RED);

        B.undo();
        checkBoard("bad8", B);
    }

    @Test
    public void clearTest() {
        Board B = new Board(5);
        B.set(2, 2, 3, RED);
        B.set(1, 1, 1, RED);
        B.set(1, 2, 2, RED);
        checkBoard("bad1", B, 1, 1, 1, RED, 2, 2, 3, RED, 1, 2, 2, RED);
        B.clear(4);
        assertEquals("bad3", 0, B.current());
        B.set(2, 2, 3, RED);
        checkBoard("bad5", B, 2, 2, 3, RED);
    }

    @Test
    public void equalTest() {
        Board B = new Board(4);
        Board C = new Board(4);
        assertEquals("bad1", true, B.equals(C));

        B.set(2, 2, 3, RED);
        assertEquals("bad2", false, B.equals(C));
    }

    /** Test for copying boards.
     * Case1: only want to copy contents of the board itself,
     * doesn't modify gamestates.
     * Also used for initializing a board using contents from another board
     * Case2: want to copy the entire board, including its state,
     * into the current board.
     */

    @Test
    public void historyTest() {
        Board B = new Board(4);
        B.set(2, 2, 3, RED);
        B.addSpot(RED, 2, 1);
        Board C = new Board(B);
        assertEquals("bad1", C.getHistory().size(), 1);
        assertEquals("bad2", B.getHistory().size(), 2);

        Board D = new Board(4);
        D.copy(B);
        checkBoard("bad7", D, 2, 2, 3, RED, 2, 1, 2, RED);
        assertEquals("bad3", 2, D.getHistory().size());
        assertEquals("bad4", RED, D.get(5).getSide());
        checkBoard("bad6", B, 2, 2, 3, RED, 2, 1, 2, RED);
        D.undo();
        checkBoard("bad5", D);
        checkBoard("bad8", B, 2, 2, 3, RED, 2, 1, 2, RED);

    }

    @Test
    public void legalTest() {
        Board B = new Board(4);
        B.set(2, 2, 3, RED);
        B.set(1, 1, 2, BLUE);
        assertEquals("bad1", false, B.isLegal(RED, 1, 1));
        assertEquals("bad2", true, B.isLegal(RED, 1, 2));
    }

    @Test
    public void markUndoTest() {
        Board B = new Board(4);
        B.set(2, 2, 3, RED);
        B.set(1, 1, 2, BLUE);
        assertEquals("bad1", false, B.getUndo());
        B.addSpot(BLUE, 1, 1);
        assertEquals("bad2", true, B.getUndo());

        Board C = new Board(B);
        assertEquals("bad3", false, C.getUndo());

        Board D = new Board(4);
        D.copy(B);
        assertEquals("bad4", true, D.getUndo());
    }

    @Test
    public void dumpTest() {
        Board B = new Board(5);
        B.set(2, 2, 3, RED);
        B.set(1, 1, 1, RED);
        B.set(1, 2, 2, RED);
        String b1 = B.toString();
        System.out.print(b1);
    }

    @Test
    public void moreJumpTest() {
        Board B = new Board(3);
        B.set(1, 1, 2, RED);
        B.set(1, 3, 2, BLUE);
        B.set(2, 2, 3, RED);
        B.set(2, 3, 3, BLUE);
        B.set(3, 3, 2, BLUE);
        B.addSpot(BLUE, 2, 3);
        B.addSpot(RED, 2, 1);
        B.addSpot(BLUE, 3, 1);
        B.addSpot(RED, 2, 1);
        B.addSpot(BLUE, 2, 2);

        System.out.print(B.toString());


    }


    /** Checks that B conforms to the description given by CONTENTS.
     *  CONTENTS should be a sequence of groups of 4 items:
     *  r, c, n, s, where r and c are row and column number of a square of B,
     *  n is the number of spots that are supposed to be there and s is the
     *  color (RED or BLUE) of the square.  All squares not listed must
     *  be WHITE with one spot.  Raises an exception signaling a unit-test
     *  failure if B does not conform. */
    private void checkBoard(String msg, Board B, Object... contents) {
        for (int k = 0; k < contents.length; k += 4) {
            String M = String.format("%s at %d %d", msg, contents[k],
                                     contents[k + 1]);
            assertEquals(M, (int) contents[k + 2],
                         B.get((int) contents[k],
                               (int) contents[k + 1]).getSpots());
            assertEquals(M, contents[k + 3],
                         B.get((int) contents[k],
                               (int) contents[k + 1]).getSide());
        }
        int c;
        c = 0;
        for (int i = B.size() * B.size() - 1; i >= 0; i -= 1) {
            assertTrue("bad white square #" + i,
                       (B.get(i).getSide() != WHITE)
                       || (B.get(i).getSpots() == 1));
            if (B.get(i).getSide() != WHITE) {
                c += 1;
            }
        }
        assertEquals("extra squares filled", contents.length / 4, c);
    }

}
