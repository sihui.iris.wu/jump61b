package jump61;

import java.util.ArrayList;
import java.util.ArrayDeque;
import java.util.Formatter;

import java.util.function.Consumer;

import static jump61.Side.*;

/** Represents the state of a Jump61 game.  Squares are indexed either by
 *  row and column (between 1 and size()), or by square number, numbering
 *  squares by rows, with squares in row 1 numbered from 0 to size()-1, in
 *  row 2 numbered from size() to 2*size() - 1, etc. (i.e., row-major order).
 *
 *  A Board may be given a notifier---a Consumer<Board> whose
 *  .accept method is called whenever the Board's contents are changed.
 *
 *  @author Iris Wu
 */
class Board {

    /** An uninitialized Board.  Only for use by subtypes. */
    protected Board() {
        _notifier = NOP;
    }

    /** An N x N board in initial configuration. */
    Board(int N) {
        this();
        _size = N;
        _board = new Square[N][N];
        for (int i = 0; i < size(); i++) {
            for (int j = 0; j < size(); j++) {
                _board[i][j] = Square.square(WHITE, 1);
            }
        }
        Square[][] curr = new Square[_size][_size];
        curr = this.copy(_board);
        _history.add(curr);
        _undo = false;
        _current = 0;

    }

    /** Copy the content of Suare[][] into another 2-D array.
     * @param sq another board content.
     * @return Square[][], a copy of the original content. */
    Square[][] copy(Square[][] sq) {
        Square[][] result = new Square[sq.length][sq.length];
        for (int i = 0; i < sq.length; i++) {
            for (int j = 0; j < sq.length; j++) {
                result[i][j] = sq[i][j];
            }
        }
        return result;
    }

    /** A board whose initial contents are copied from BOARD0, but whose
     *  undo history is clear, and whose notifier does nothing. */
    Board(Board board0) {
        this(board0.size());
        this.internalCopy(board0);
        _history.clear();
        Square[][] curr = new Square[board0.size()][board0.size()];
        curr = this.copy(_board);
        _history.add(curr);
        _readonlyBoard = new ConstantBoard(this);
    }

    /** Returns a readonly version of this board. */
    Board readonlyBoard() {
        return _readonlyBoard;
    }

    /** (Re)initialize me to a cleared board with N squares on a side. Clears
     *  the undo history and sets the number of moves to 0. */
    void clear(int N) {
        _board = new Square[N][N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                _board[i][j] = Square.square(WHITE, 1);
            }
        }
        _size = N;
        _current = 0;
        _history.clear();
        announce();
    }

    /** Copy the contents of BOARD into me. */
    void copy(Board board) {
        if (board.size() != this.size()) {
            throw new GameException("wrong size board");
        }
        this.internalCopy(board);
        _readonlyBoard = new ConstantBoard(this);
        this._undo = board.getUndo();
        this._history = board._history;
        this._current = board._current;
    }

    /** Copy the contents of BOARD into me, without modifying my undo
     *  history. Assumes BOARD and I have the same size. */
    private void internalCopy(Board board) {
        assert size() == board.size();
        for (int i = 1; i <= size(); i++) {
            for (int j = 1; j <= size(); j++) {
                _board[i - 1][j - 1] = board.get(sqNum(i, j));
            }
        }
    }

    /** Return the number of rows and of columns of THIS. */
    int size() {
        return _size;
    }

    /** Returns the contents of the square at row R, column C
     *  1 <= R, C <= size (). */
    Square get(int r, int c) {
        return _board[r - 1][c - 1];
    }

    /** Returns the contents of square #N, numbering squares by rows, with
     *  squares in row 1 number 0 - size()-1, in row 2 numbered
     *  size() - 2*size() - 1, etc. */
    Square get(int n) {
        int row = n / size();
        int col = n % size();
        return _board[row][col];
    }

    /** Returns the total number of spots on the board.
     * iterates through the entire board, getting number of spots in each
     * square. */
    int numPieces() {
        int result = 0;
        for (int i = 1; i <= size(); i++) {
            for (int j = 1; j <= size(); j++) {
                result += get(sqNum(i, j)).getSpots();
            }
        }
        return result;
    }

    /** Returns the Side of the player who would be next to move.  If the
     *  game is won, this will return the loser (assuming legal position). */
    Side whoseMove() {
        return ((numPieces() + size()) & 1) == 0 ? RED : BLUE;
    }

    /** Return true iff row R and column C denotes a valid square. */
    final boolean exists(int r, int c) {
        return 1 <= r && r <= size() && 1 <= c && c <= size();
    }

    /** Return true iff S is a valid square number. */
    final boolean exists(int s) {
        int N = size();
        return 0 <= s && s < N * N;
    }

    /** Return the row number for square #N. */
    final int row(int n) {
        return n / size() + 1;
    }

    /** Return the column number for square #N. */
    final int col(int n) {
        return n % size() + 1;
    }

    /** Return the square number of row R, column C. */
    final int sqNum(int r, int c) {
        return (c - 1) + (r - 1) * size();
    }

    /** Return a string denoting move (ROW, COL)N. */
    String moveString(int row, int col) {
        return String.format("%d %d", row, col);
    }

    /** Return a string denoting move N. */
    String moveString(int n) {
        return String.format("%d %d", row(n), col(n));
    }

    /** Returns true iff it would currently be legal for PLAYER to add a spot
        to square at row R, column C. */
    boolean isLegal(Side player, int r, int c) {
        return isLegal(player, sqNum(r, c));
    }

    /** Returns true iff it would currently be legal for PLAYER to add a spot
     *  to square #N. */
    boolean isLegal(Side player, int n) {
        return (get(n).getSide().equals(WHITE)
                || get(n).getSide().equals(player));
    }

    /** Returns true iff PLAYER is allowed to move at this point. */
    boolean isLegal(Side player) {
        if (whoseMove().equals(player)) {
            return true;
        }
        return false;
    }

    /** Returns the winner of the current position, if the game is over,
     *  and otherwise null. */
    final Side getWinner() {
        Side side = get(1, 1).getSide();
        if (side.equals(WHITE)) {
            return null;
        }
        for (int i = 1; i <= size(); i++) {
            for (int j = 1; j <= size(); j++) {
                Square temp = get(i, j);
                if (!temp.getSide().equals(side)) {
                    return null;
                }
            }
        }
        return side;
    }

    /** Return the number of squares of given SIDE. */
    int numOfSide(Side side) {
        int result = 0;
        for (int i = 1; i <= size(); i++) {
            for (int j = 1; j <= size(); j++) {
                Square temp = get(i, j);
                if (temp.getSide().equals(side)) {
                    result += temp.getSpots();
                }
            }
        }
        return result;
    }

    /** Add a spot from PLAYER at row R, column C.  Assumes
     *  isLegal(PLAYER, R, C). */
    void addSpot(Side player, int r, int c) {
        if (!isLegal(player, r, c)) {
            throw new IllegalStateException("illegal player state");
        }
        if (!isLegal(player)) {
            throw new GameException("wrong side");
        }
        addSpot(player, sqNum(r, c));

    }

    /** Add a spot from PLAYER at square #N.  Assumes isLegal(PLAYER, N). */
    void addSpot(Side player, int n) {
        if (!isLegal(player, n)) {
            throw new GameException("illegal player state");
        }
        if (neighbors(n) == 4) {
            if (get(n).getSpots() < 4) {
                simpleAdd(player, n, 1);
            } else {
                jump(n);
            }
            _current += 1;
            Square[][] curr = new Square[_size][_size];
            curr = this.copy(_board);
            _history.add(curr);
            markUndo();
        } else if (neighbors(n) == 3) {
            if (get(n).getSpots() < 3) {
                simpleAdd(player, n, 1);
            } else {
                jump(n);
            }
            _current += 1;
            Square[][] curr = new Square[_size][_size];
            curr = this.copy(_board);
            _history.add(curr);
            markUndo();
        } else if (neighbors(n) == 2) {
            if (get(n).getSpots() < 2) {
                simpleAdd(player, n, 1);
            } else {
                jump(n);
            }
            _current += 1;
            Square[][] curr = new Square[_size][_size];
            curr = this.copy(_board);
            _history.add(curr);
            markUndo();
        } else {
            throw new GameException("invalid player number");
        }
    }

    /** Set the square at row R, column C to NUM spots (0 <= NUM), and give
     *  it color PLAYER if NUM > 0 (otherwise, white). */
    void set(int r, int c, int num, Side player) {
        internalSet(r, c, num, player);
        announce();
    }

    /** Set the square at row R, column C to NUM spots (0 <= NUM), and give
     *  it color PLAYER if NUM > 0 (otherwise, white).  Does not announce
     *  changes. */
    private void internalSet(int r, int c, int num, Side player) {
        internalSet(sqNum(r, c), num, player);
    }

    /** Set the square #N to NUM spots (0 <= NUM), and give it color PLAYER
     *  if NUM > 0 (otherwise, white). Does not announce changes. */
    private void internalSet(int n, int num, Side player) {
        int row = n / size();
        int col = n % size();
        _board[row][col] = Square.square(player, num);
    }

    /** Undo the effects of one move (that is, one addSpot command).  One
     *  can only undo back to the last point at which the undo history
     *  was cleared, or the construction of this Board. */
    void undo() {
        if (_undo) {
            _history.remove(current());
            _current -= 1;
            _board = new Square[size()][size()];
            _board = this.copy(_history.get(_current));

        }
    }

    /** Record the beginning of a move in the undo history. */
    private void markUndo() {
        _undo = true;
    }

    /** Returns the current state of the board. */
    int current() {
        return _current;
    }

    /** Add DELTASPOTS spots of side PLAYER to row R, column C,
     *  updating counts of numbers of squares of each color. */
    private void simpleAdd(Side player, int r, int c, int deltaSpots) {
        internalSet(r, c, deltaSpots + get(r, c).getSpots(), player);
    }

    /** Add DELTASPOTS spots of color PLAYER to square #N,
     *  updating counts of numbers of squares of each color. */
    private void simpleAdd(Side player, int n, int deltaSpots) {
        internalSet(n, deltaSpots + get(n).getSpots(), player);
    }

    /** Used in jump to keep track of squares needing processing.  Allocated
     *  here to cut down on allocations. */
    private final ArrayDeque<Integer> _workQueue = new ArrayDeque<>();

    /** Do all jumping on this board, assuming that initially, S is the only
     *  square that might be over-full. */
    private void jump(int S) {
        if (!(this.getWinner() == null)) {
            return;
        }
        Side side = get(S).getSide();
        internalSet(S, 1, side);
        int[] neighbor = neighborOf(S);
        for (int n:neighbor) {
            if (neighbors(n) == 4) {
                if (get(n).getSpots() < 4) {
                    simpleAdd(side, n, 1);
                } else {
                    simpleAdd(side, n, 0);
                    jump(n);
                }
            } else if (neighbors(n) == 3) {
                if (get(n).getSpots() < 3) {
                    simpleAdd(side, n, 1);
                } else {
                    simpleAdd(side, n, 0);
                    jump(n);
                }
            } else if (neighbors(n) == 2) {
                if (get(n).getSpots() < 2) {
                    simpleAdd(side, n, 1);
                } else {
                    simpleAdd(side, n, 0);
                    jump(n);
                }
            }
        }
    }

    /** Returns the neighbor of a square.
     * @param s a given square position. */
    int[] neighborOf(int s) {
        int[] result = new int[neighbors(s)];
        int row = row(s);
        int col = col(s);
        if (row == 1  || row == size() || col == 1 || col == size()) {
            if (row == 1 && col == 1) {
                result[0] = s + size();
                result[1] = s + 1;
            } else if (row == 1 && col == size()) {
                result[0] = s + size();
                result[1] = s - 1;
            } else if (row == size() && col == 1) {
                result[0] = s - size();
                result[1] = s + 1;
            } else if (row == size() && col == size()) {
                result[0] = s - size();
                result[1] = s - 1;
            } else if (row == 1) {
                result[0] = s - 1;
                result[1] = s + 1;
                result[2] = s + size();
            } else if (row == size()) {
                result[0] = s - 1;
                result[1] = s + 1;
                result[2] = s - size();
            } else if (col == 1) {
                result[0] = s - size();
                result[1] = s + size();
                result[2] = s + 1;
            } else if (col == size()) {
                result[0] = s + size();
                result[1] = s - size();
                result[2] = s - 1;
            }
        } else {
            result[0] = s - 1;
            result[1] = s + 1;
            result[2] = s - size();
            result[3] = s + size();
        }
        return result;
    }

    /** Returns my dumped representation. */
    @Override
    public String toString() {
        Formatter out = new Formatter();
        out.format("===");
        for (int i = 1; i <= size(); i++) {
            out.format("\n    ");
            for (int j = 1; j <= size(); j++) {
                Square sq = get(i, j);
                if (j == size()) {
                    out.format(getStringSpots(sq) + getStringSide(sq));
                } else {
                    out.format(getStringSpots(sq) + getStringSide(sq) + " ");
                }
            }
        }
        out.format("\n===");
        return out.toString();
    }

    /** returns the intended representation for a give square.
     * @param sq a certain square.
     * */
    String getStringSide(Square sq) {
        Side side = sq.getSide();
        if (side == RED) {
            return "r";
        } else if (side == BLUE) {
            return "b";
        } else {
            return "-";
        }
    }

    /** Returns the intended representation for number of spots in a square.
     * @param sq a certain square. */
    String getStringSpots(Square sq) {
        int i = sq.getSpots();
        return String.valueOf(i);
    }

    /** Returns an external rendition of me, suitable for human-readable
     *  textual display, with row and column numbers.  This is distinct
     *  from the dumped representation (returned by toString). */
    public String toDisplayString() {
        String[] lines = toString().trim().split("\\R");
        Formatter out = new Formatter();
        for (int i = 1; i + 1 < lines.length; i += 1) {
            out.format("%2d %s%n", i, lines[i].trim());
        }
        out.format("  ");
        for (int i = 1; i <= size(); i += 1) {
            out.format("%3d", i);
        }
        return out.toString();
    }

    /** Returns the number of neighbors of the square at row R, column C. */
    int neighbors(int r, int c) {
        int size = size();
        int n;
        n = 0;
        if (r > 1) {
            n += 1;
        }
        if (c > 1) {
            n += 1;
        }
        if (r < size) {
            n += 1;
        }
        if (c < size) {
            n += 1;
        }
        return n;
    }

    /** Returns the number of neighbors of square #N. */
    int neighbors(int n) {
        return neighbors(row(n), col(n));
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Board)) {
            return false;
        } else {
            Board B = (Board) obj;
            for (int i = 0; i < size() * size(); i++) {
                int currNum = get(i).getSpots();
                Side currColor = get(i).getSide();
                if (currNum != B.get(i).getSpots()
                        || currColor != B.get(i).getSide()) {
                    return false;
                }
            }
            return true;
        }
    }

    @Override
    public int hashCode() {
        return numPieces();
    }

    /** Set my notifier to NOTIFY. */
    public void setNotifier(Consumer<Board> notify) {
        _notifier = notify;
        announce();
    }

    /** Take any action that has been set for a change in my state. */
    private void announce() {
        _notifier.accept(this);
    }

    /** A notifier that does nothing. */
    private static final Consumer<Board> NOP = (s) -> { };

    /** Returns the history of the board. */
    ArrayList<Square[][]> getHistory() {
        return _history;
    }

    /** Returns the current state of board. */
    Square[][] getBoard() {
        return _board;
    }

    /** Returns the current _undo state. */
    boolean getUndo() {
        return _undo;
    }

    /** A read-only version of this Board. */
    private ConstantBoard _readonlyBoard;

    /** Use _notifier.accept(B) to announce changes to this board. */
    private Consumer<Board> _notifier;

    /** size of the board. */
    private int _size;

    /** Empty contents of the board. */
    private Square[][] _board;

    /** History of all the previous boards. */
    private ArrayList<Square[][]> _history = new ArrayList<>();

    /** Records the current index of the game state. */
    private int _current;

    /** Mark whether it is ok to undo or not under the current game state. */
    private boolean _undo;

}
