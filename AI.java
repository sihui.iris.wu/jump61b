package jump61;

import java.util.Random;

import static jump61.Side.*;

/** An automated Player.
 *  @author P. N. Hilfinger
 */
class AI extends Player {

    /** A new player of GAME initially COLOR that chooses moves automatically.
     *  SEED provides a random-number seed used for choosing moves.
     */
    AI(Game game, Side color, long seed) {
        super(game, color);
        _random = new Random(seed);
    }

    @Override
    String getMove() {
        Board board = getGame().getBoard();
        assert getSide() == board.whoseMove();
        int choice = searchForMove();
        getGame().reportMove(board.row(choice), board.col(choice));
        return String.format("%d %d", board.row(choice), board.col(choice));
    }

    /** Return an integer that indicates a winningValue
     * under certain kind of evaluation.
     * @param side side of a player
     * @param b a given board
     * @return an integer for winningmove.
     */
    private int winningValue(Side side, Board b) {
        if (side == BLUE) {
            return -(b.size() * b.size());
        }
        return b.size() * b.size();
    }

    /** Return a move after searching the game tree to DEPTH>0 moves
     *  from the current position. Assumes the game is not over. */
    private int searchForMove() {
        Board work = new Board(getBoard());
        int value;
        assert getSide() == work.whoseMove();
        _foundMove = -1;
        if (getSide() == RED) {
            value = minMax(work, 4, true,
            redSense, minimum, maximum);
        } else {
            value = minMax(work, 4, true,
            blueSense, minimum, maximum);
        }
        return _foundMove;
    }

    /** Find a move from position BOARD and return its value, recording
     *  the move found in _foundMove iff SAVEMOVE. The move
     *  should have maximal value or have value > BETA if SENSE==1,
     *  and minimal value or value < ALPHA if SENSE==-1. Searches up to
     *  DEPTH levels.  Searching at level 0 simply returns a static estimate
     *  of the board value and does not set _foundMove. If the game is over
     *  on BOARD, does not set _foundMove. */
    private int minMax(Board board, int depth, boolean saveMove,
                       int sense, int alpha, int beta) {
        Side side = board.whoseMove();
        Board temp = new Board(board);
        if (depth == 0 || temp.getWinner() != null) {
            if (side == RED) {
                return staticEval(temp, winningValue(RED, temp));
            }
            return staticEval(temp, winningValue(BLUE, temp));
        }
        int best = 0;
        if (sense == 1) {
            best = minimum;
            for (int i = 0; i < temp.size() * temp.size(); i++) {
                if (temp.get(i).getSide().equals(side)
                        || temp.get(i).getSide().equals(WHITE)) {
                    temp.addSpot(side, i);
                    int response = minMax(temp, depth - 1,
                            false, -1, alpha, beta);
                    if (response > best) {
                        best = response;
                        alpha = Math.max(best, response);
                        if (saveMove) {
                            _foundMove = i;
                        }
                        if (alpha >= beta) {
                            return best;
                        }
                    }
                    temp.undo();
                }

            }
        } else if (sense == -1) {
            best = maximum;
            for (int i = 0; i < temp.size() * temp.size(); i++) {
                if (temp.get(i).getSide().equals(side)
                        || temp.get(i).getSide().equals(WHITE)) {
                    temp.addSpot(side, i);
                    int response = minMax(temp, depth - 1,
                            false, 1, alpha, beta);
                    if (response < best) {
                        best = response;
                        beta = Math.min(best, beta);
                        if (saveMove) {
                            _foundMove = i;
                        }
                        if (alpha >= beta) {
                            return best;
                        }
                    }
                    temp.undo();
                }
            }

        } else {
            throw new GameException("illegal sense number");
        }
        return best;
    }

    /** Return a heuristic estimate of the value of board position B.
     *  Use WINNINGVALUE to indicate a win for Red and -WINNINGVALUE to
     *  indicate a win for Blue. */
    private int staticEval(Board b, int winningValue) {
        Side side = b.whoseMove();
        if (side == RED) {
            return b.numOfSide(RED);
        }
        return -b.numOfSide(BLUE);
    }


    /** A random-number generator used for move selection. */
    private Random _random;

    /** Used to convey moves discovered by minMax. */
    private int _foundMove;

    /** arbitrary number set for alpha value. */
    private final int maximum = 1000;

    /** arbitrary number set for beta value. */
    private final int minimum = -1000;

    /** arbitrary number set for red sense value. */
    private final int redSense = 1;

    /** arbitrary number set for blue sense value. */
    private final int blueSense = -1;
}
